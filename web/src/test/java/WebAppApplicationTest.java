import com.epam.mentoring.timetracker.web.TimeTrackerWebApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TimeTrackerWebApplication.class)
@WebAppConfiguration
public class WebAppApplicationTest {

	@Test
	public void contextLoads() {
	}

}
