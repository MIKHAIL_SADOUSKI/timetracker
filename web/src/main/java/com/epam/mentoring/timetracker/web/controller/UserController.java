package com.epam.mentoring.timetracker.web.controller;

import com.epam.mentoring.timetracker.model.TrackPoint;
import com.epam.mentoring.timetracker.model.User;
import com.epam.mentoring.timetracker.service.TrackPointService;
import com.epam.mentoring.timetracker.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private TrackPointService trackPointService;

    @RequestMapping(value = "/user/{id}/trackpoint", method = RequestMethod.POST)
    public String saveTrackPoint(@PathVariable  Long id, TrackPoint trackPoint, Model model) {
        TrackPoint savedTrackPoint = trackPointService.save(trackPoint);
        User user = userService.findOne(id);

        LOGGER.debug("Save trackpoint: userId = {}, trackPointId = {}", user.getId(), savedTrackPoint.getId());

        if (user != null) {
            user.getTrackpoints().add(savedTrackPoint);
            user = userService.save(user);
        }

        model.addAttribute("user", user);
        model.addAttribute("trackpoint", new TrackPoint());

        return "userform";
    }


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showUsers(Model model) {
        List<User> users = userService.findAll();
        double totalUsersTime = users.stream().mapToDouble(user -> user.getTrackpoints().stream().mapToDouble(TrackPoint::getTrackingTime).sum()).sum();
        LOGGER.debug("Users were retrieved in user controller. TotalUsersTime: {}", totalUsersTime);

        model.addAttribute("totalUsersTime", totalUsersTime);
        model.addAttribute("users", users);

        return "users";
    }


    @RequestMapping("user/{id}")
    public String showUser(@PathVariable Long id, Model model) {
        User user = userService.findOne(id);
        if (user != null) {
            double totalTime = user.getTrackpoints().stream().mapToDouble(trackPoint->trackPoint.getTrackingTime()).sum();
            model.addAttribute("totalTime", totalTime);
            LOGGER.debug("User were retrieved in user controller. Total Time: {}", totalTime);
        }
        model.addAttribute("user", user);

        return "usershow";
    }

    @RequestMapping("user/edit/{id}")
    public String editUser(@PathVariable Long id, Model model) {
        model.addAttribute("user", userService.findOne(id));
        model.addAttribute("trackpoint", new TrackPoint());

        return "userform";
    }

    @RequestMapping("user/new")
    public String newUser(Model model) {
        model.addAttribute("user", new User());

        return "userCreationForm";
    }

    @RequestMapping(value = "user", method = RequestMethod.POST)
    public String saveUser(User user) {
        userService.save(user);

        return "redirect:/user/" + user.getId();
    }

    @RequestMapping("user/delete/{id}")
    public String deleteUser(@PathVariable Long id) {
        userService.delete(id);

        return "redirect:/users";
    }

}
