package com.epam.mentoring.timetracker.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.epam" })
@PropertySource({"classpath:application-devdb.properties"})
@EnableJpaRepositories(basePackages = {"com.epam"})
public class TimeTrackerWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeTrackerWebApplication.class, args);
    }
}
