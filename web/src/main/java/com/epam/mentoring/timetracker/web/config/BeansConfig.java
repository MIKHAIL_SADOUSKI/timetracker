package com.epam.mentoring.timetracker.web.config;

import com.epam.mentoring.timetracker.service.TrackPointService;
import com.epam.mentoring.timetracker.service.UserService;
import com.epam.mentoring.timetracker.service.impl.TrackPointServiceImpl;
import com.epam.mentoring.timetracker.service.impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class BeansConfig {

    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    public TrackPointService trackPointService() {
        return new TrackPointServiceImpl();
    }

}
