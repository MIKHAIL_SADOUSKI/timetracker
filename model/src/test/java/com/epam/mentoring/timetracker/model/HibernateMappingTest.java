package com.epam.mentoring.timetracker.model;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import static org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = PersistenceConfig.class)
@ActiveProfiles("devdb")
@AutoConfigureTestDatabase(replace = NONE)
public class HibernateMappingTest {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Test
    public void testMappings() {
        Map<String, Exception> errors = new HashMap<>();

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        sessionFactory.getAllClassMetadata().values().forEach(classMetadata -> {
            try {
                sessionFactory.getCurrentSession().createCriteria(classMetadata.getMappedClass()).setMaxResults(1).list();
            } catch (Exception e) {
                errors.put(classMetadata.getEntityName(), e);
            }
        });

        Assert.state(errors.size() == 0, buildErrorMessage(errors));
    }

    private static String buildErrorMessage(Map<String, Exception> errors) {
        StringBuilder builder = new StringBuilder();
        builder.append("Following errors occurred");

        errors.forEach((name, exception) -> {
            builder.append("\n");
            builder.append(name);
            builder.append(" : ");
            builder.append(exception.getCause() != null ? exception.getCause() : exception);
        });

        return builder.toString();
    }
}
