package com.epam.mentoring.timetracker.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan("com.epam.mentoring.timetracker.model")
public class PersistenceConfig {

}
