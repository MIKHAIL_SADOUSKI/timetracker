package com.epam.mentoring.timetracker.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;

@Entity(name = "users")
@AttributeOverride(name = "id", column = @Column(name = "user_id"))
public class User extends GenericEntity {
    private String firstName;
    private String lastName;
    private String department;
    private List<TrackPoint> trackPoints;

    @Column(name = "first_name", nullable = false, length = 45)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name", nullable = false, length = 45)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "department", nullable = false, length = 100)
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {this.department = department;}

    @ManyToMany
    @JoinTable(name = "users_trackpoints_map", joinColumns = { @JoinColumn(name = "user_id", nullable = false) },
            inverseJoinColumns = { @JoinColumn(name = "trackpoint_id", nullable = false) })
    public List<TrackPoint> getTrackpoints() {
        return trackPoints;
    }

    public void setTrackpoints(List<TrackPoint> trackpoints) {
        this.trackPoints = trackpoints;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("department", department)
                .append("trackpoints", trackPoints)
                .build();
    }
}
