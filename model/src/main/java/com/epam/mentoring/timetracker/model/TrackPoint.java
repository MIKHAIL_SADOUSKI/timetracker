package com.epam.mentoring.timetracker.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity(name = "trackpoints")
@AttributeOverride(name = "id", column = @Column(name = "trackpoint_id"))
public class TrackPoint extends GenericEntity {
    private LocalDate trackingDate;
    private Double trackingTime;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "trackpoint_date", nullable = false)
    public LocalDate getTrackingDate() {
        return trackingDate;
    }

    public void setTrackingDate(LocalDate trackingDate) {
        this.trackingDate = trackingDate;
    }

    @Column(name = "trackpoint_time", nullable = false)
    public Double getTrackingTime() {
        return trackingTime;
    }

    public void setTrackingTime(Double trackingTime) {
        this.trackingTime = trackingTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("trackingDate", trackingDate)
                .append("trackingTime", trackingTime)
                .build();
    }
}
