CREATE DATABASE timetracker;
USE timetracker;

CREATE TABLE IF NOT EXISTS `timetracker`.`trackpoints` (
  `trackpoint_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `trackpoint_time` DECIMAL(10, 2) NOT NULL,
  `trackpoint_date` DATE NOT NULL,
  PRIMARY KEY (`trackpoint_id`),
  UNIQUE INDEX `trackpoint_id_UNIQUE` (`trackpoint_id` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `timetracker`.`users` (
  `user_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `department` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `timetracker`.`users_trackpoints_map` (
  `id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(10) NOT NULL,
  `trackpoint_id` BIGINT(10) NOT NULL,
  INDEX `fk_users_trackpoints1_idx` (`user_id` ASC),
  INDEX `fk_users_trackpoints2_idx` (`trackpoint_id` ASC),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_users_trackpoints1`
    FOREIGN KEY (`trackpoint_id`)
    REFERENCES `timetracker`.`trackpoints` (`trackpoint_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_trackpoints2`
    FOREIGN KEY (`user_id`)
    REFERENCES `timetracker`.`users` (`user_id`))
ENGINE = InnoDB;
