package com.epam.mentoring.timetracker.dao;

import com.epam.mentoring.timetracker.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends PagingAndSortingRepository<User, Long> {

}
