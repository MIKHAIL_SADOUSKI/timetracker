package com.epam.mentoring.timetracker.dao;

import com.epam.mentoring.timetracker.model.TrackPoint;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackPointDao extends PagingAndSortingRepository<TrackPoint, Long> {

}
