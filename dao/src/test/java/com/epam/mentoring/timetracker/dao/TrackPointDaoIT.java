package com.epam.mentoring.timetracker.dao;

import com.epam.mentoring.timetracker.model.TrackPoint;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class TrackPointDaoIT extends AbstractIT {

    private static final LocalDate LOCAL_DATE = LocalDate.now();
    private static final Double TIME = 8.0;

    @Autowired
    private TrackPointDao dao;

    @Test
    public void findOne() {
        TrackPoint expected = buildTrackPoint();
        save(expected);
        flushAndClear();

        assertTrackPoint(dao.findOne(expected.getId()), expected);
    }

    @Test
    public void findAll() {
        TrackPoint trackPoint1 = buildTrackPoint();
        TrackPoint trackPoint2 = buildTrackPoint();
        List<TrackPoint> expectedTrackPoints = Arrays.asList(trackPoint1, trackPoint2);

        save(trackPoint1);
        save(trackPoint2);
        flushAndClear();

        int size = Math.toIntExact(dao.count());

        List<TrackPoint> actualTrackPoints = StreamSupport.stream(dao.findAll().spliterator(), false)
                .collect(Collectors.toList());

        assertThat("TrackPoint page is expected", actualTrackPoints, is(notNullValue()));
        assertThat("Invalid total size", actualTrackPoints.size(), is(size));
        MatcherAssert.assertThat(getIds(actualTrackPoints), is(getIds(expectedTrackPoints)));
    }

    @Test
    public void saveNew() {
        TrackPoint expected = buildTrackPoint();

        dao.save(expected);
        flushAndClear();

        assertTrackPoint(findTrackPoint(expected.getId()), expected);
    }

    @Test
    public void update() {
        Long trackPointId = save(buildTrackPoint());
        TrackPoint trackPoint = findTrackPoint(trackPointId);
        flushAndClear();

        trackPoint.setTrackingTime(10.0);

        dao.save(trackPoint);

        assertTrackPoint(findTrackPoint(trackPoint.getId()), trackPoint);
    }

    @Test
    public void delete() {
        Long trackPointId = save(buildTrackPoint());
        flushAndClear();

        dao.delete(trackPointId);
        flushAndClear();

        assertThat("No trackPoint expected", findTrackPoint(trackPointId), is(nullValue()));
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteNonexistent() {
        dao.delete(0L);
    }

    private TrackPoint buildTrackPoint() {
        TrackPoint trackPoint = new TrackPoint();
        trackPoint.setTrackingDate(LOCAL_DATE);
        trackPoint.setTrackingTime(TIME);

        return trackPoint;
    }

    private void assertTrackPoint(TrackPoint actual, TrackPoint expected) {
        assertThat("TrackPoint is expected", actual, is(notNullValue()));
        assertThat("Invalid id", actual.getId(), is(expected.getId()));
        assertThat("Invalid trackingDate", actual.getTrackingDate(), is(expected.getTrackingDate()));
        assertThat("Invalid trackingTime", actual.getTrackingTime(), is(expected.getTrackingTime()));
    }

    private TrackPoint findTrackPoint(Long id) {
        return findById(TrackPoint.class, id);
    }
}
