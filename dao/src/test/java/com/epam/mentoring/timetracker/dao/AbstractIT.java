package com.epam.mentoring.timetracker.dao;

import com.epam.mentoring.timetracker.model.GenericEntity;
import com.epam.mentoring.timetracker.model.PersistenceConfig;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = PersistenceConfig.class)
@ActiveProfiles("devdb")
@AutoConfigureTestDatabase(replace = NONE)
public abstract class AbstractIT {

    @Autowired
    private TestEntityManager entityManager;

    public <T extends GenericEntity> T findById(Class<T> entityClass, Long entityId) {
        return entityManager.find(entityClass, entityId);
    }

    public Long save(GenericEntity entity) {
        return entityManager.persistAndGetId(entity, Long.class);
    }

    public <T extends GenericEntity> List<Long> getIds(List<T> entities) {
        if (entities == null) {
            return Collections.emptyList();
        }

        return entities.stream()
                .map(GenericEntity::getId)
                .collect(Collectors.toList());
    }

    void flushAndClear() {
        entityManager.flush();
        entityManager.clear();
    }
}
