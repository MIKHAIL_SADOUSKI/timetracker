package com.epam.mentoring.timetracker.dao;

import com.epam.mentoring.timetracker.model.User;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class UserDaoIT extends AbstractIT {

    private static final String FIRST_NAME = "first name";
    private static final String LAST_NAME = "last name";
    private static final String DEPARTMENT = "department";

    @Autowired
    private UserDao dao;

    @Test
    public void findOne() {
        User expected = buildUser();
        save(expected);
        flushAndClear();

        assertUser(dao.findOne(expected.getId()), expected);
    }

    @Test
    public void findAll() {
        User user1 = buildUser();
        User user2 = buildUser();
        List<User> expectedUsers = Arrays.asList(user1, user2);

        save(user1);
        save(user2);
        flushAndClear();

        int size = Math.toIntExact(dao.count());

        List<User> actualUsers = StreamSupport.stream(dao.findAll().spliterator(), false)
                .collect(Collectors.toList());

        assertThat("User page is expected", actualUsers, is(notNullValue()));
        assertThat("Invalid total size", actualUsers.size(), is(size));
        MatcherAssert.assertThat(getIds(actualUsers), is(getIds(expectedUsers)));
    }

    @Test
    public void saveNew() {
        User expected = buildUser();

        dao.save(expected);
        flushAndClear();

        assertUser(findUser(expected.getId()), expected);
    }

    @Test
    public void update() {
        Long userId = save(buildUser());
        User user = findUser(userId);
        flushAndClear();

        dao.save(user);

        assertUser(findUser(user.getId()), user);
    }

    @Test
    public void delete() {
        Long userId = save(buildUser());
        flushAndClear();

        dao.delete(userId);
        flushAndClear();

        assertThat("No user expected", findUser(userId), is(nullValue()));
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteNonexistent() {
        dao.delete(0L);
    }

    private User buildUser() {
        User user = new User();
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setDepartment(DEPARTMENT);
        user.setTrackpoints(Collections.emptyList());

        return user;
    }

    private void assertUser(User actualUser, User expectedUser) {
        assertThat("User is expected", actualUser, is(notNullValue()));
        assertThat("Invalid id", actualUser.getId(), is(expectedUser.getId()));
        assertThat("Invalid first name", actualUser.getFirstName(), is(expectedUser.getFirstName()));
        assertThat("Invalid last name", actualUser.getLastName(), is(expectedUser.getLastName()));
        assertThat("Invalid department", actualUser.getDepartment(), is(expectedUser.getDepartment()));
        assertThat("Invalid trackpoints", getIds(actualUser.getTrackpoints()), is(getIds(expectedUser.getTrackpoints())));
    }


    private User findUser(Long id) {
        return findById(User.class, id);
    }
}
