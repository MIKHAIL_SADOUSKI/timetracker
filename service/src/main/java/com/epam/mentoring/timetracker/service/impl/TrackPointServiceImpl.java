package com.epam.mentoring.timetracker.service.impl;

import com.epam.mentoring.timetracker.dao.TrackPointDao;
import com.epam.mentoring.timetracker.model.TrackPoint;
import com.epam.mentoring.timetracker.service.TrackPointService;
import com.epam.mentoring.timetracker.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class TrackPointServiceImpl implements TrackPointService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackPointServiceImpl.class);

    @Autowired
    private TrackPointDao trackPointDao;

    @Override
    public TrackPoint save(TrackPoint trackPoint) {
        if (trackPoint == null) {
            LOGGER.error("Null arguments are provided for trackpoint creation. TrackPoint: {}", trackPoint);
            throw new ServiceException("Null arguments are provided for trackpoint creation. TrackPoint: " + trackPoint);
        }

        TrackPoint savedTrackPoint = trackPointDao.save(trackPoint);

        LOGGER.debug("TrackPoint {} was successfully saved", trackPoint);

        return savedTrackPoint;
    }

    @Override
    public TrackPoint findOne(Long trackPointId) {
        if (trackPointId == null) {
            LOGGER.error("Unable to retrieve trackpoint by null id");
            throw new ServiceException("Unable to retrieve trackpoint by null id");
        }

        TrackPoint trackPoint = trackPointDao.findOne(trackPointId);

        LOGGER.debug("TrackPoint is retrieved: {}", trackPoint);

        return trackPoint;
    }

    @Override
    public List<TrackPoint> findAll() {
        List<TrackPoint> trackPoints = StreamSupport.stream(trackPointDao.findAll().spliterator(), false)
                .collect(Collectors.toList());

        LOGGER.debug("TrackPoint(s) was successfully received. Size - {}", trackPoints.size());

        return trackPoints;
    }

    @Override
    public void delete(Long trackPointId) {
        if (trackPointId == null) {
            LOGGER.error("Unable to delete trackpoint with null id");
            throw new ServiceException("Unable to delete trackpoint with null id");
        }

        trackPointDao.delete(trackPointId);

        LOGGER.info("TrackPoint with id = {} was successfully deleted", trackPointId);
    }
}
