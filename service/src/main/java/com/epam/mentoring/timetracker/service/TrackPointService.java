package com.epam.mentoring.timetracker.service;

import com.epam.mentoring.timetracker.model.TrackPoint;

import java.util.List;

public interface TrackPointService {

    TrackPoint save(TrackPoint trackPoint);

    TrackPoint findOne(Long trackPointId);

    List<TrackPoint> findAll();

    void delete(Long trackPointId);

}
