package com.epam.mentoring.timetracker.service.impl;

import com.epam.mentoring.timetracker.dao.UserDao;
import com.epam.mentoring.timetracker.model.User;
import com.epam.mentoring.timetracker.service.UserService;
import com.epam.mentoring.timetracker.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Override
    public User save(User user) {
        if (user == null) {
            LOGGER.error("Null arguments are provided for user creation. User: {}", user);
            throw new ServiceException("Null arguments are provided for user creation. User: " + user);
        }

        User savedUser = userDao.save(user);

        LOGGER.debug("User {} was successfully saved", savedUser);

        return savedUser;
    }

    @Override
    public List<User> createBatch(List<User> users, String roleName) {
        if (users == null || roleName == null) {
            LOGGER.error("Null arguments are provided for creation of users. Users: {}, roleName: {}", users, roleName);
            throw new ServiceException("Null arguments are provided for creation of users. Users: " + users + ", roleName: " + roleName);
        }

        List<User> filteredUsers = users.stream()
                .filter(Objects::nonNull)
                .collect(toList());

        if (filteredUsers.isEmpty()) {
            LOGGER.warn("Empty collection of users was provided for creation");

            return filteredUsers;
        }

        List<User> savedUsers = StreamSupport.stream(userDao.save(filteredUsers).spliterator(), false)
                .collect(Collectors.toList());

        LOGGER.debug("Users {} were successfully created", savedUsers);

        return savedUsers;
    }

    @Override
    public User findOne(Long userId) {
        if (userId == null) {
            LOGGER.error("Unable to retrieve user by null id");
            throw new ServiceException("Unable to retrieve user by null id");
        }

        User user = userDao.findOne(userId);

        LOGGER.debug("User is retrieved: {}", user);

        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> users = StreamSupport.stream(userDao.findAll().spliterator(), false)
                .collect(Collectors.toList());

        LOGGER.debug("User(s) was successfully received. Size - {}", users.size());

        return users;
    }

    @Override
    public void delete(Long userId) {
        if (userId == null) {
            LOGGER.error("Unable to delete user with null id");
            throw new ServiceException("Unable to delete user with null id");
        }

        userDao.delete(userId);

        LOGGER.info("User with id = {} was successfully deleted", userId);
    }

}
