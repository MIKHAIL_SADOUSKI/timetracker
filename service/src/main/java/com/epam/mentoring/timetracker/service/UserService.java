package com.epam.mentoring.timetracker.service;

import com.epam.mentoring.timetracker.model.User;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> createBatch(List<User> users, String roleName);

    User findOne(Long userId);

    List<User> findAll();

    void delete(Long userId);

}
