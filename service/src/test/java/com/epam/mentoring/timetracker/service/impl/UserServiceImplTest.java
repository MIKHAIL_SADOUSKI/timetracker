package com.epam.mentoring.timetracker.service.impl;

import com.epam.mentoring.timetracker.dao.UserDao;
import com.epam.mentoring.timetracker.model.User;
import com.epam.mentoring.timetracker.service.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.never;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl service;

    @Mock
    private UserDao dao;

    @Test
    public void save() {
        //given
        User expectedUser = buildUser();
        given(dao.save(any(User.class))).willReturn(expectedUser);

        //when
        User actualUser = service.save(new User());

        //then
        assertThat(actualUser, is(notNullValue()));
        assertUser(actualUser, expectedUser);
        then(dao).should().save(any(User.class));
    }

    @Test(expected = ServiceException.class)
    public void saveNullableArgument() {
        try {
            //when
            service.save(null);
        } finally {
            //then
            then(dao).should(never()).save(any(User.class));
        }
    }

    @Test
    public void delete() {
        //given
        willDoNothing().given(dao).delete(any(Long.class));

        //when
        service.delete(5L);

        //then
        then(dao).should().delete(any(Long.class));
    }

    @Test(expected = ServiceException.class)
    public void deleteNullableArgument() {
        try {
            //when
            service.delete(null);
        } finally {
            //then
            then(dao).should(never()).delete(anyLong());
        }
    }

    private void assertUser(User actual, User expected) {
        assertThat(actual.getId(), is(expected.getId()));
        assertThat(actual.getFirstName(), is(expected.getFirstName()));
        assertThat(actual.getLastName(), is(expected.getLastName()));
        assertThat(actual.getDepartment(), is(expected.getDepartment()));
        assertThat(actual.getTrackpoints().size(), is(expected.getTrackpoints().size()));
    }

    private User buildUser() {
        User user = new User();
        user.setFirstName("firstname");
        user.setLastName("lastname");
        user.setDepartment("department");
        user.setTrackpoints(Collections.emptyList());

        return user;
    }
}
