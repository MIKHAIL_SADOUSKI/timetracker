package com.epam.mentoring.timetracker.service.impl;

import com.epam.mentoring.timetracker.dao.TrackPointDao;
import com.epam.mentoring.timetracker.model.TrackPoint;
import com.epam.mentoring.timetracker.service.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.never;

@RunWith(MockitoJUnitRunner.class)
public class TrackPointServiceImplTest {

    @InjectMocks
    private TrackPointServiceImpl service;

    @Mock
    private TrackPointDao dao;

    @Test
    public void save() {
        //given
        TrackPoint expectedTrackPoint = buildTrackPoint();
        given(dao.save(any(TrackPoint.class))).willReturn(expectedTrackPoint);

        //when
        TrackPoint actualTrackPoint = service.save(new TrackPoint());

        //then
        assertThat(actualTrackPoint, is(notNullValue()));
        assertTrackPoint(actualTrackPoint, expectedTrackPoint);
        then(dao).should().save(any(TrackPoint.class));
    }

    @Test(expected = ServiceException.class)
    public void saveNullableArgument() {
        try {
            //when
            service.save(null);
        } finally {
            //then
            then(dao).should(never()).save(any(TrackPoint.class));
        }
    }

    @Test
    public void delete() {
        //given
        willDoNothing().given(dao).delete(any(Long.class));

        //when
        service.delete(5L);

        //then
        then(dao).should().delete(any(Long.class));
    }

    @Test(expected = ServiceException.class)
    public void deleteNullableArgument() {
        try {
            //when
            service.delete(null);
        } finally {
            //then
            then(dao).should(never()).delete(anyLong());
        }
    }

    private void assertTrackPoint(TrackPoint actual, TrackPoint expected) {
        assertThat(actual.getId(), is(expected.getId()));
        assertThat(actual.getTrackingDate(), is(expected.getTrackingDate()));
        assertThat(actual.getTrackingTime(), is(expected.getTrackingTime()));
    }

    private TrackPoint buildTrackPoint() {
        TrackPoint trackPoint = new TrackPoint();
        trackPoint.setTrackingDate(LocalDate.now());
        trackPoint.setTrackingTime(2D);

        return trackPoint;
    }
}
